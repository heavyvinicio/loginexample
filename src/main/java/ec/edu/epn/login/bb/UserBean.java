/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.epn.login.bb;

import ec.edu.epn.login.enums.RoleEnum;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.enterprise.context.Dependent;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Vinicio
 */
@Dependent
public class UserBean implements Serializable {

    private static final long serialVersionUID = -8943703610229516408L;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private boolean administratorRole;
    @Getter
    @Setter
    private boolean userRole;

    @Getter
    @Setter
    private RoleEnum role;
    @Setter
    @Getter
    private List<RoleEnum> roles;
    @Getter
    @Setter
    private Date loginDate;

    public UserBean() {
        setLoginDate(new Date());
        roles = new ArrayList<RoleEnum>();
    }

}
