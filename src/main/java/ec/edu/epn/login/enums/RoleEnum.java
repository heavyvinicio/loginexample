/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.epn.login.enums;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Vinicio
 */
public enum RoleEnum {
    ROLE_ADMIN("admin", "The user can create, edit or delete many things.", "/private/admin/index.jsf"), ROLE_USER("user", "The user view some things.", "/private/user/index.jsf");

    @Getter
    @Setter
    private String role;
    @Getter
    @Setter
    private String url;
    @Getter
    @Setter
    private String description;

    private RoleEnum(String role, String description, String url) {
        this.role = role;
        this.url = url;
        this.description = description;
    }

    public static final RoleEnum obtainRole(String role) {
        for (RoleEnum r : RoleEnum.values()) {
            if (r.getRole().equals(role)) {
                return r;
            }
        }
        return null;
    }
}
