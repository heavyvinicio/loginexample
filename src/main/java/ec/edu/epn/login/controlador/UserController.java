/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.epn.login.controlador;

import ec.edu.epn.login.bb.UserBean;
import ec.edu.epn.login.enums.RoleEnum;
import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import org.apache.shiro.SecurityUtils;

/**
 *
 * @author Vinicio adss
 */
@Named
@SessionScoped
public class UserController implements Serializable {
    
    private static final long serialVersionUID = 9155110911896732120L;
    @Inject
    @Getter
    @Setter
    private UserBean userBean;
    
    @PostConstruct
    private void init() {
        
        validateUserLogin();
        
    }
    
    private void validateUserLogin() {
        if (SecurityUtils.getSubject().isAuthenticated()) {
            getUserBean().setUsername(SecurityUtils.getSubject().getPrincipal().toString());
            
            if (SecurityUtils.getSubject().hasRole(RoleEnum.ROLE_ADMIN.getRole())) {
                
                getUserBean().setAdministratorRole(true);
                getUserBean().getRoles().add(RoleEnum.ROLE_ADMIN);
            }
            if (SecurityUtils.getSubject().hasRole(RoleEnum.ROLE_USER.getRole())) {
                getUserBean().getRoles().add(RoleEnum.ROLE_USER);
                getUserBean().setUserRole(true);
                
            }
        }
    }
    
    public void validateUrl(String role) {
        
        getUserBean().setRole(RoleEnum.obtainRole(role));
        redirectUrl(getUserBean().getRole().getUrl());
        
    }
    
    private void redirectUrl(String url) {
        try {
            
            FacesContext.getCurrentInstance().getExternalContext().redirect(FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath() + url);
            
        } catch (IOException ex) {
            Logger.getLogger(UserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void logout() {
        SecurityUtils.getSubject().logout();
        redirectUrl("/logout.jsf");
    }
    public void returnMainMenu(){
        redirectUrl("/private/index.jsf");
    }
    public void returnHome(){
        redirectUrl("/");
    }
}
