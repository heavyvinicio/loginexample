<%-- 
    Document   : login
    Created on : 27-dic-2015, 13:28:47
    Author     : Vinicio
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"></meta>

        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link  href="${pageContext.servletContext.contextPath}/resources/default/css/style.css" rel='stylesheet' type='text/css'/>
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text.css'/>
        
        <title>JSP Page</title>
    </head>
    <body>
        <%! String errorMessage = null;%>
        <%
            errorMessage = (String) request.getAttribute("loginFailure");
            if (errorMessage != null) { %>
        <font color="red">Invalid Login</font><br/>

        <% } %>
        <div class="main">
            <form method="POST" action=""  >
                <h1><span>Registro</span> <lable> Login </lable> </h1>
                <div class="inset">
                    <p>
                        <label for="email">Usuario</label>
                        <input required="true"  name="username" type="text" />

                    </p>
                    <p>
                        <label for="password">PASSWORD</label>
                        <input required="true"  name="password" type="password" />

                    </p>
                    <p>
                        <input type="checkbox" name="rememberMe"  id="remember"/>
                        <label for="remember">Remember me for 14 days</label>
                    </p>
                </div>

                <p class="p-container">
                    <span><a href="#">Forgot password ?</a></span>
                    <input type="submit"  value="Login" />

                </p>
            </form> 

        </div>
        <div id="test" class="copy-right">

            <p> &#169; 2015 Login form example. All rights reserved | Template by <a href="http://w3layouts.com/">W3layouts</a></p> 
        </div>
    </body>
</html>
