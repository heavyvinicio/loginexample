# README #

This project contains un example the form authentication with apache shiro y java (JSF y CDI).
<br/>If you like this, please help me to improve with anything.
### Compiling ###
In a terminal execute:<br/>
* mvn clean install<br/>
### Deploy ###

* You need glasfish server


### Test Users ###
You can probe with:<br/>
User | password<br/>
admin  admin<br/>
hulk   test123<br/>
<br/>
Thanks for probe.